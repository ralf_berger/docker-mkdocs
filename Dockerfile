FROM python:3-alpine

RUN apk --no-cache add build-base libffi-dev jpeg-dev zlib-dev cairo \
  musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf

ADD requirements.txt .
RUN pip install -q --no-cache-dir -r requirements.txt

EXPOSE 8000

WORKDIR /docs
ENTRYPOINT ["mkdocs"]
CMD ["--help"]
