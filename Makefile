DOCKER_IMAGE = regreb/mkdocs

build:
	docker build -t $(DOCKER_IMAGE) .

push: test
	docker push $(DOCKER_IMAGE)

test:
	bundle exec rspec

setup:
	bundle

shell:
	docker run -it --rm --entrypoint /bin/sh $(DOCKER_IMAGE)

all: setup build push

.PHONY: build push test init all
